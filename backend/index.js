const express = require('express');
const cors = require('cors');
const {nanoid} = require("nanoid");

const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};

let pixelsArray = [];

app.ws('/canvas', (ws, req) => {
    const id = nanoid();
    activeConnections[id] = ws;
    console.log('Client connected', id);

    console.log(ws['pixelsArray']);

    if (pixelsArray) {
        ws.send(JSON.stringify({
            type: 'NEW_USER_SESSION',
            pixelsArray,
        }));
    }

    ws.on('message', msg => {
        const decoded = JSON.parse(msg);
        switch (decoded.type) {
            case 'CREATE_PIXELS_ARRAY':
                Object.keys(activeConnections).forEach(key => {
                    if (key !== id) {
                        const wsConnection = activeConnections[key];
                        wsConnection.send(JSON.stringify({
                            type: 'UPDATE_PIXELS_ARRAY',
                            pixelsArray: decoded.pixelsArray
                        }));
                        pixelsArray = decoded.pixelsArray; // additional
                    }
                });
                break;
            default:
                console.log('Unknown type');
        }
    });

    ws.on('close', () => {
        console.log('Client', id, 'disconnected');
        delete activeConnections[id];
    });
});

app.listen(port, () => {
    console.log(`Sever started on port ${port} port !`);
});

