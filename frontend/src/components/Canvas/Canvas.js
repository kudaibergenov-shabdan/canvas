import React, {useState, useRef, useEffect} from 'react';

const Canvas = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    const canvas = useRef(null);

    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            if (decoded.type === 'UPDATE_PIXELS_ARRAY') {
                setState(state => ({
                    ...state,
                    pixelsArray: decoded.pixelsArray,
                }));
                decoded.pixelsArray.forEach(coordinate => {
                    draw({
                        clientX: coordinate.x,
                        clientY: coordinate.y
                    });
                });
            }

            // when new user connected
            if(decoded.type === 'NEW_USER_SESSION') {
                setState(state => ({
                    ...state,
                    pixelsArray: decoded.pixelsArray,
                }));
                decoded.pixelsArray.forEach(coordinate => {
                    draw({
                        clientX: coordinate.x,
                        clientY: coordinate.y
                    });
                });
            }
        }
    }, []);

    const draw = event => {
        const clientX = event.clientX;
        const clientY = event.clientY;
        setState(prevState => {
            return {
                ...prevState,
                pixelsArray: [...prevState.pixelsArray, {
                    x: clientX,
                    y: clientY
                }]
            };
        });
        const context = canvas.current.getContext('2d');
        const imageData = context.createImageData(1, 1);
        const d = imageData.data;
        d[0] = 0;
        d[1] = 0;
        d[2] = 0;
        d[3] = 255;
        context.putImageData(imageData, event.clientX, event.clientY);
    };

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            //event.persist();
            draw(event);
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        ws.current.send(JSON.stringify({
            type: 'CREATE_PIXELS_ARRAY',
            pixelsArray: [...state.pixelsArray],
        }));

        setState({...state, mouseDown: false, pixelsArray: []});
    };

    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default Canvas;